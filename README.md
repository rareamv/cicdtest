# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This will be used to test CI/CD using a custom CI/CD application that listens to commits through the BitBucket API. It doesn't use the webhook function as a tunnle for local usage is required.


### How do I get set up? ###

Currently,the application doesnt exist. But, the general idea is:

* Add username and token for bitbucket (and possible other version management platforms) through webinterface.
* Select a repo to listen to. 
* Add a build script with build steps (first command line stuff, but later through graphical interface?)
* Add console output through webclient. 

Future plans:

* Add one click install for sdk, runtimes, compilers, etc. (Much like visual studio market). 
* Add auto feature detect (sdk, runtimes, compilers, etc.).
* Add multiple build steps.
* Add multi repo/account/project support.